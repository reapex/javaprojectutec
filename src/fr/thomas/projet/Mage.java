package fr.thomas.projet;

public class Mage extends Personnage {

	public Mage(String name, String length, int attack, int health) {
		super(name, length, attack, health);
	}

	@Override
	public void special() {
		int randNum = (int) (Math.random() * 9);
		if (randNum == 0) {
			health = health + 25;
			System.out.println(name + " viens d'utiliser son ultime ! Il a désormais " + health + " points de vie.\n");
		}
	}
}