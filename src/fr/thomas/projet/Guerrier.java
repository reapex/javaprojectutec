package fr.thomas.projet;

public class Guerrier extends Personnage {

	public Guerrier(String name, String length, int attack, int health) {
		super(name, length, attack, health);
	}

	@Override
	public void special() {
		int randNum = (int) (Math.random() * 9);
		if (randNum == 0) {
			attack = attack + 3;
			System.out.println(name + " viens d'utiliser son ultime ! Il a d�sormais " + attack
					+ " points d'attaque par d�faut.\n");
		}
	}

}