package fr.thomas.projet;

public class Archer extends Personnage {

	public Archer(String name, String length, int attack, int health) {
		super(name, length, attack, health);
	}

	@Override
	public void special() {
		int randNum = (int) (Math.random() * 9);
		if (randNum == 0) {
			health = health + attack;
			System.out.println(name + " viens d'utiliser son ultime ! Il a désormais " + health + " points de vie.\n");
		}
	}

}
