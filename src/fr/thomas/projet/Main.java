/*
 * Auteur : Thomas ROBERT
 * Projet : Combat entre deux personnages de diff�rentes classes
 * Gitlab : https://gitlab.com/reapex/javaprojectutec
 */

package fr.thomas.projet;

public class Main {

	/*
	 * Caract�ristique des classses par d�faut :
	 * 
	 * Mage : nom, taille, attaque par d�faut: 16, point de vie par d�faut: 290.
	 * Guerrier : nom, taille, attaque par d�faut: 22, point de vie par d�faut: 250.
	 * Archer : nom, taille, attaque par d�faut: 18, point de vie par d�faut: 275.
	 * 
	 * Attaques sp�ciale par classe :
	 * 
	 * Mage : R�g�n�ration de points de vie. Guerrier : Augmentation de l'attaque de
	 * base. Archer : Ajout des points d'attaque aux points de vie .
	 * 
	 */

	public static void main(String[] args) {

		// D�but de l'histoire.
		System.out.println(">>> Bienvenue � vous dans l'Ar�ne ! <<<");
		System.out.println("");
		delay(5000);

		// Choix al�atoire de la classe du joueur.
		String[] personnages = { "Mage", "Guerrier", "Archer" };
		int randNum = (int) (Math.random() * personnages.length);
		String persoJoueur = personnages[randNum];

		// Cr�ation du personnage selon sa classe.
		switch (persoJoueur) {

		// Case Mage
		case "Mage":
			Mage mage = new Mage(createName("Mage"), "1m" + createLength("Mage"), 16, 290);
			System.out.println("Vous �tes un Mage.\nVotre nom est : " + mage.getName() + ".\nVous mesurez "
					+ mage.getLength() + ".");

			// Choix de l'adversaire
			String[] opponents = { "Guerrier", "Archer" };
			int OpponentName = (int) (Math.random() * opponents.length);
			String opponent = opponents[OpponentName];
			System.out.println("Vous affrontez un " + opponent + " !");
			System.out.println("");
			delay(5000);

			// Cr�ation de l'adversaire
			if (opponent == "Guerrier") {
				Guerrier opp = new Guerrier(createName("Guerrier"), "1m" + createLength("Guerrier"), 16, 290);
				fight(mage, opp);
			} else {
				Archer opp = new Archer(createName("Archer"), "1m" + createLength("Archer"), 18, 275);
				fight(mage, opp);
			}
			break;

		// Case Guerrier
		case "Guerrier":
			Guerrier guerrier = new Guerrier(createName("Guerrier"), "1m" + createLength("Guerrier"), 22, 250);
			System.out.println("Vous �tes un Guerrier.\nVotre nom est : " + guerrier.getName() + ".\nVous mesurez "
					+ guerrier.getLength() + ".");

			// Choix de l'adversaire
			String[] opponents1 = { "Mage", "Archer" };
			int OpponentName1 = (int) (Math.random() * opponents1.length);
			String opponent1 = opponents1[OpponentName1];
			System.out.println("Vous affrontez un " + opponent1 + " !");
			System.out.println("");
			delay(5000);

			// Cr�ation de l'adversaire
			if (opponent1 == "Mage") {
				Mage opp = new Mage(createName("Mage"), "1m" + createLength("Mage"), 16, 290);
				fight(guerrier, opp);
			} else {
				Archer opp = new Archer(createName("Archer"), "1m" + createLength("Archer"), 18, 275);
				fight(guerrier, opp);
			}
			break;

		// Case Archer
		case "Archer":
			Archer archer = new Archer(createName("Archer"), "1m" + createLength("Archer"), 18, 275);
			System.out.println("Vous �tes un Archer.\nVotre nom est : " + archer.getName() + ".\nVous mesurez "
					+ archer.getLength() + ".");

			// Choix de l'adversaire
			String[] opponents2 = { "Guerrier", "Mage" };
			int OpponentName2 = (int) (Math.random() * opponents2.length);
			String opponent2 = opponents2[OpponentName2];
			System.out.println("Vous affrontez un " + opponent2 + " !");
			System.out.println("");
			delay(5000);

			// Cr�ation de l'adversaire
			if (opponent2 == "Mage") {
				Mage opp = new Mage(createName("Mage"), "1m" + createLength("Mage"), 16, 290);
				fight(archer, opp);
			} else {
				Guerrier opp = new Guerrier(createName("Guerrier"), "1m" + createLength("Guerrier"), 22, 250);
				fight(archer, opp);
			}
			break;
		}

	}

	// Cr�ation du nom du personnage en fonction de sa classe.
	private static String createName(String classe) {

		String PlayerName = null;

		switch (classe) {

		case "Mage":
			String[] names = { "Bregolas", "Isildur", "Nerdanel" };
			int randNum = (int) (Math.random() * names.length);
			PlayerName = names[randNum];
			break;

		case "Guerrier":
			String[] names1 = { "Andras", "Deryn", "Rhys" };
			int randNum1 = (int) (Math.random() * names1.length);
			PlayerName = names1[randNum1];
			break;

		case "Archer":
			String[] names2 = { "Brindal", "Daenara", "Estrid" };
			int randNum2 = (int) (Math.random() * names2.length);
			PlayerName = names2[randNum2];
			break;
		}

		return PlayerName; // On retourne le nom al�atoire du personnage.

	}

	// Cr�ation de la taille du personnage
	private static int createLength(String classe) {

		int PlayerLength = 0;

		switch (classe) {

		case "Mage":
			int randNum = (int) (Math.random() * 38);
			PlayerLength = 60 + randNum;
			break;

		case "Guerrier":
			int randNum1 = (int) (Math.random() * 41);
			PlayerLength = 55 + randNum1;
			break;

		case "Archer":
			int randNum2 = (int) (Math.random() * 41);
			PlayerLength = 50 + randNum2;
			break;
		}

		return PlayerLength; // On retourne le nom al�atoire du personnage.
	}

	// Coinflip.
	private static int coinflip() {
		int coinflip = 0;
		String[] faces = { "Pile", "Face" };
		int face = (int) (Math.random() * faces.length);
		String finalFace = faces[face];
		if (finalFace == "Pile") {
			System.out.println("Le r�sultat est Pile, Votre adversaire attaque en premier !");
			coinflip = 1;
		} else {
			System.out.println("Le r�sultat est Face, vous attaquez en premier !");
		}
		return coinflip;
	}

	// Domage final (domage de base + bonus par tour).
	private static int finalDamage(Personnage perso) {
		int randNum = (int) (Math.random() * 9);
		int damage = (int) (perso.getAttack() + randNum);
		return damage;
	}

	// Cr�ation du combat
	private static void fight(Personnage player1, Personnage player2) {
		System.out.println(">>> Le combat va commencer... <<<");
		delay(7500);
		System.out.println("");
		System.out.println("Le combat oppose : " + player1.getName() + " vs " + player2.getName() + " !");
		System.out.println("");
		delay(5000);
		System.out.println("Pour savoir qui attaque en premier, nous allons faire un pile ou face !");
		delay(2000);
		System.out.println("Vous gagnez avec Face...");

		// D�but du combat.
		delay(5000);
		System.out.println("");
		System.out.println(">>> D�but du combat ... <<<");
		System.out.println("");
		delay(2000);

		if (coinflip() == 0) {
			while (player1.getHealth() > 0 && player2.getHealth() > 0) {

				System.out.println("");
				delay(3000);
				player2.special();
				player2.damage(finalDamage(player1));
				if (player2.getHealth() <= 0) {
					System.out.println("");
					System.out.println(player2.getName() + " n'a plus de point de vie, le vainqueur est "
							+ player1.getName() + " !");
					delay(2000);
					System.out.println("\n{ Code source : https://gitlab.com/reapex/javaprojectutec }");
					delay(10000);
					return;
				}
				System.out.println("");
				delay(3000);
				player1.special();
				player1.damage(finalDamage(player2));
				if (player1.getHealth() <= 0) {
					System.out.println("");
					System.out.println(player1.getName() + " n'a plus de point de vie, le vainqueur est "
							+ player2.getName() + " !");
					delay(2000);
					System.out.println("\n{ Code source : https://gitlab.com/reapex/javaprojectutec }");
					delay(10000);
					return;
				}

			}

		} else {
			while (player1.getHealth() > 0 && player2.getHealth() > 0) {

				System.out.println("");
				delay(3000);
				player1.special();
				player1.damage(finalDamage(player2));
				if (player1.getHealth() <= 0) {
					System.out.println("");
					System.out.println(">>> " + player1.getName() + " n'a plus de points de vie, le vainqueur est "
							+ player2.getName() + " ! <<<");
					delay(2000);
					System.out.println("\n{ Code source : https://gitlab.com/reapex/javaprojectutec }");
					delay(10000);
					return;
				}
				System.out.println("");
				delay(3000);
				player2.special();
				player2.damage(finalDamage(player1));
				if (player2.getHealth() <= 0) {
					System.out.println("");
					System.out.println(">>> " + player2.getName() + " n'a plus de points de vie, le vainqueur est "
							+ player1.getName() + " ! <<<");
					delay(2000);
					System.out.println("\n{ Code source : https://gitlab.com/reapex/javaprojectutec }");
					delay(10000);
					return;
				}
			}

		}

	}

	// Fonction pour ajouter du d�lai
	private static void delay(int sec) {
		try {
			Thread.sleep(sec);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}