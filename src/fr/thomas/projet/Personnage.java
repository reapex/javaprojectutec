package fr.thomas.projet;

public abstract class Personnage {

	protected String name;
	protected String length;
	protected int health;
	protected int attack;

	public Personnage(String name, String length, int attack, int health) {
		this.name = name;
		this.attack = attack;
		this.health = health;
		this.length = length;
	}

	// Mthode  redfinir.
	public void special() {

	}

	public void damage(int damage) {
		this.health -= damage;
		System.out.println(name + " viens de subir " + damage + " dgts ! Il lui reste " + health + " points de vie.");

	}

	// Getter & Setter

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getHealth() {
		return health;
	}

	public void setHealth(int health) {
		this.health = health;
	}

	public double getAttack() {
		return attack;
	}

	public void setAttack(int attack) {
		this.attack = attack;
	}

	public String getLength() {
		return length;
	}

	public void setLength(String length) {
		this.length = length;
	}

}
